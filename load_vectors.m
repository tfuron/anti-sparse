% This scripts generates a set of vectors, 
% and compute the corresponding ground-truth (= true Euclidean neighbors)

pca = false;  % pca is used only for real vectors

if strcmp (dataset, 'randb')
  d = 16;
  n = 10000;          % number of database vectors
  nq = 1000;         % number of query vectors
  nl = 10000;        % number of vectors used for learning
  m = 48;            % number of bits produced by the representation

  vb = randn (d, n);   % base vectors (to be searched in)
  vq = randn (d, nq);  % query vectors
  vl = randn (d, nl);
  vb = fvecs_normalize (vb);
  vq = fvecs_normalize (vq);
  vl = fvecs_normalize (vl);

  % compute/load the ground-truth
  [gnd, dis] = nn (vb, vq, 1);

% 
elseif strcmp (dataset, 'randn')
  d = 16;
  n = 10000;          % number of database vectors
  nq = 1000;         % number of query vectors
  nl = 1000;         % number of vectors used for learning
  m = 48;            % number of bits produced by the representation

  vb = randn (d, n);   % base vectors (to be searched in)
  vq = randn (d, nq);  % query vectors
  vl = randn (d, nl);

  % compute/load the ground-truth
  [gnd, dis] = nn (vb, vq, 1);

end


% PCA is not used in this package (used for real vectors only)
if pca
  vl = double (vl);
  vecmean = mean (vl, 2);
  vb = vb - repmat (vecmean, 1, n);
  vq = vq - repmat (vecmean, 1, nq);
  vl = vl - repmat (vecmean, 1, nl);
 
  Xcov = vl * vl';
  Xcov = (Xcov + Xcov') / 2;
  
  [eigvec, eigval] = eigs (Xcov, pcadim, 'LM');
  
  % Project the vectors
  vb = eigvec' * vb;
  vq = eigvec' * vq;
  vl = eigvec' * vl;
  d = pcadim;
end