% function  [x,n_iter,q] = l2_h_linf_final(y,A,h)
%
% This function minizes the functional
% 1/2 ||Ax-y||_2^2 + h * ||x||_inf
% within n_iter iterations
%
% The solution is a vector x whose components are s.t. |x_j| = +/- ||x||_inf
% except q (<=n_iter) of them which are in (-1,1)*||x||_inf
%
% Inputs:
% * y: vector  [mx1]
% * A: matrix  [mxn]
% * h: scalar  [1x1]
%
% Outputs:
% * x: solution vector [nx1]
% * n_iter: number of iterations [1x1]
% * q: number of components in (-1,1)*||x||_inf
function  [x,n_iter,q] = l2_h_linf_final_test(y,A,h)
%*************************************************************************
%%                   Initialization
%*************************************************************************
n_iter=0; % Nb of iteration
q = 0;
Abar = A;
n_x=size(Abar,2); % Length of x
if h < 1.e-8
  h = 1.e-8;
end
flag_good_interval = false;

% for h large enough...
% Sect. 2.2
hk = +Inf;
x = zeros(n_x,1);
% When is the next jump?
% for xinf small enough...
% Sect. 2.2
hkplus1 = norm(Abar'*y,1); % next critical value of h
if h > hkplus1
  flag_good_interval = true;   % Stop here
  % h is so big that zeros(n_x,1) is solution
else
  % Be prepared for the next jump
  Ibar = 1:n_x;
  sign_vbar = sign(Abar'*y);
  Abreve = zeros(size(Abar,1),0);
  Ibreve = zeros(1,0);
  xinfkplus1 = 0;
  q = 0;
end
%**************************************************************************
%%                                Main loop
%**************************************************************************
while (flag_good_interval == false)
  n_iter = n_iter+1;
  %% prepare some auxiliary data
  B_tmp = inv(Abreve'*Abreve);
  Abarxsignvbar = Abar*sign_vbar;
  C_tmp = Abarxsignvbar'*Abreve;
  % xbreve = xi + zeta*xinf (Eq.(11))
  xi = B_tmp*Abreve'*y; % Eq.(12)
  zeta = -B_tmp*C_tmp'; % Eq.(13)
  % h vbar = nu + mu*xinf (Eq.(14))
  mu = -Abar'*(Abreve*zeta+Abarxsignvbar); %Eq.(15)
  nu = Abar'*(y-Abreve*xi); % Eq.(16)
  % h = eta + nu*xinf (Eq.(17))
  upsilon = -C_tmp*zeta- Abarxsignvbar'*Abarxsignvbar; %Eq.(18)
  eta = -C_tmp*xi+Abarxsignvbar'*y; % Eq.(19)
  % scalars
  hk = hkplus1;
  xinfk = xinfkplus1;
  
  %% Where is the next jump?
  %---------------------------
  % Case 1: a component of xbreve reaches +-xinf
  %---------------------------
  %       Case 1.a: reaches +xinf
  i1_pos = 0;
  xinf_pos = inf;
  test = xi./(1-zeta);
  ind = find(test >xinfk+1.e-8 );
  if size(ind,1) > 0
    [xinf_pos,i3] = min(test(ind));
    i1_pos=ind(i3);
  end
  %       Case 1.b: reaches -xinf
  i1_neg=0;
  xinf_neg= inf;
  test=-xi./(1+zeta);
  ind=find(test >xinfk+1.e-8 );
  if size(ind,1)>0
    [xinf_neg,i3]=min(test(ind));
    i1_neg= ind(i3);
  end
  %       Summary: take the smallest one
  xinf1=inf;
  if (i1_neg>0) || (i1_pos>0)
    if xinf_pos < xinf_neg
      xinf1= xinf_pos;
      i1 = i1_pos;
      sx1 = +1;
    else
      xinf1 = xinf_neg;
      i1 = i1_neg;
      sx1 = -1;
    end
  end
  %-------------------------
  % Case 2: a component of h*vbar reaches 0
  %----------------------------
  indd = find(abs(mu)>1.e-5);
  test = -nu(indd)./mu(indd);
  j1 = 0;
  xinf2 = inf;
  ind = find(test >xinfk+1.e-8 );
  if size(ind,1) > 0
    [xinf2,j3] = min(test(ind));
    j1 = indd(ind(j3)); % v(j1) cancels and xinf = xinf2
  end
  %-------------------------
  % Synthesis of cases 1 & 2
  %----------------------------
  % Select the smallest possible value for xinf but > current xinf
  xinfkplus1 = min(xinf1,xinf2);
  hkplus1 = eta+upsilon*xinfkplus1;  % corresponding value for h (Eq.(17))
  
  if (h<hk) && (h >= hkplus1) % Solve the problem if h in this interval
    flag_good_interval = true;
    xinfk = (h-eta)/upsilon; %corresponding value for xinf (Eq.(17))
    xbreve = xi+zeta*xinfk; % Eq.(11) 
    [Y,I] = sort([Ibreve' ;Ibar'],1);
    XX = [xbreve(1:q,:) ; sign_vbar*xinfk];
    x = XX(I);
    % This is the end, my only friend.
  else
    % Be prepared for the next jump
    if  xinf1<xinf2
      % xbreve(i1,1) reaches xinfk with the sign of sx1
      q = q-1; % decrease the nb of stuck components
      % Update Abar, Abreve, sign_vbar, Ibar, Ibreve
      Abar=[Abar Abreve(:,i1)]; 
      Abreve(:,i1)=[]; 
      sign_vbar=[sign_vbar ; sx1];
      Ibar=[Ibar Ibreve(i1)];
      Ibreve(i1)=[];
    else
      % a new component of x lies inside the interval
      q=q+1; % increase the nb of stuck components
      % Update Abar, Abreve, sign_vbar, Ibar, Ibreve
      Abreve=[Abreve Abar(:,j1)];
      Abar(:,j1)=[];
      sign_vbar(j1,:)=[];
      Ibreve=[Ibreve Ibar(j1)];
      Ibar(j1)=[];
    end
  end
  
end  % while flag_good_interval











