% Binarize the input vectors (and provides some values before thresholding)

function [vb, vp] = binarize_vec (v, C, method, params)

switch method
  
 case 'lsh'
  vp = (C' * v);
  
 case 'antisparse'
  m = size (C, 2);
  n = size (v, 2);
  vp = zeros (m, n);
  v = double (v);
  for i = 1:n
    if mod (i,100) == 0 
            fprintf ('\r%d/%d', i, n);
    end
    [x, ipas, dumm] = l2_h_linf_final(v(:,i), C, params.h);
    vp(:, i) = x;
  end
  fprintf ('\n\n');
  
  % Divide by the max to get values between -1 and 1
  vp = vp ./ repmat (max (abs (vp)), m, 1);
end


vb = sign (vp);

pos0 = find (vb == 0);
vb(pos0) = 1;
