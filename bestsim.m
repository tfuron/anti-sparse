% Return the order list of nearest neighbor of the vector of q in database b
% according to cosine measure (i.e., =ranking provided by Euclidean distance
% if the vectors are normalized, and =ranking provided by Hamming vectors)
function [ip,idx] = bestsim (b, q)

m = size (b, 1);

ip = q' * b;
[ip, idx] = sort (ip, 2, 'descend');
idx = idx'; 

