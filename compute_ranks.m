% This file takes a 1-NN ground-truth and a set of results to produce
% the ranks obtained for the nearest neighbors
function ranks = compute_ranks (gnd, idx)

nq = size (gnd, 2);

ranks = zeros (nq, 1);
for i = 1:nq
  pos = find (idx(:, i) == gnd(i));
  if size (pos) == 1
    ranks(i) = pos;
  end
end
