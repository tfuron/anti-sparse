% Generate a set of normalized vectors (frame) using different methods
% method:
%  'km'     use a kmeans clustering to produce m vectors, then normalize
%  'randn'  just random vectors on the sphere
%  'jj'     the method of Jean-Jacques Fuchs to produce a tight frame
function A = gen_proj (d, m, method)

if strcmp (method, 'randn') 
  A = randn (d, m);
  A = fvecs_normalize (A);
  
elseif strcmp (method, 'frame')
  A = randn (m, d);
  [Q, R] = qr (A);
  A = Q (1:d, :);
else
  disp 'Unknown frame construction method'
  assert (0);
end

A = double (A);

