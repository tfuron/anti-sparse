% Parameters

% 'randn' | 'randb' 
%  which means 
%  1) randn: gaussian
%  2) randb: uniform on the unit ball (drawn with gaussian+L2 normalization)
dataset = 'randb';                 

maxnq = 1000;     % maximum number of queries
load_vectors;     % load/generate the set of vectors

params.h = 1;     % Regularization parameter for anti-sparse coding

% Learn the codebook C. All vectors are normalized
% Methods:
%  'randn'     random normalized vector (on Euclidean unit ball)
%  'frame'     a frame obtained by QR decomposition on a random sample (best)
Clsh = gen_proj (d, m, 'randn');
C = gen_proj (d, m, 'frame');  

% LSH binarization
[binb_lsh, b_lsh] = binarize_vec (vb, Clsh, 'lsh');
[binq_lsh, q_lsh] = binarize_vec (vq, Clsh, 'lsh');

fprintf ('* Regular LSH search based on Hamming distance and random projections\n');
[ip1,idx1] = bestsim (binb_lsh, binq_lsh);

fprintf ('* LSH search based on Hamming distance and frames\n');
[binb_lshf, b_lshf] = binarize_vec (vb, C, 'lsh');
[binq_lshf, q_lshf] = binarize_vec (vq, C, 'lsh');
[ip1f,idx1f] = bestsim (binb_lshf, binq_lshf);

% Anti-sparse binarization
fprintf ('* Encode database vectors using spread representation\n');
[binb_as, b_as] = binarize_vec (vb, C, 'antisparse', params);

fprintf ('* Encode query vectors using spread representation\n');
[binq_as, q_as] = binarize_vec (vq, C, 'antisparse', params);

fprintf ('* Search using anti-sparse binarized representations\n');
[ip2,idx2] = bestsim (binb_as, binq_as);

fprintf ('* Same, but query is not quantized (values between -1 and 1)\n');
[ip3,idx3] = bestsim (binb_as, q_as);


fprintf ('* Use the explicit reconstruction of the database vectors\n');
brec = C * binb_as;
brec = fvecs_normalize (brec);
qrec = fvecs_normalize (vq);
[ip4,idx4] = bestsim (brec, qrec);


% Compute scores
rank1 = compute_ranks (gnd, idx1);
rank1f = compute_ranks (gnd, idx1f);
rank2 = compute_ranks (gnd, idx2);
rank3 = compute_ranks (gnd, idx3);
rank4 = compute_ranks (gnd, idx4);


fprintf ('\n* Results\nrecall@%-5s = %7s  / %9s /  %7s  /  %7s  /  %7s\n', ...
	 'r', 'LSH', 'LSH+frame', 'as', 'as-asym', 'as-recons');

recalls = [1 2 5 10 20 50 100 200 500 1000]; 
allres = zeros (0, 5);
for r = recalls
  resatr = [length(find (rank1 <= r)), length(find (rank1f <= r)), length(find (rank2 <= r)), ...
	   length(find (rank3 <= r)), length(find (rank4 <= r))] / nq;
   
  fprintf ('recall@%-5d = %.5f  /  %.5f  /  %.5f  /  %.5f  /  %.5f\n', ...
	   r, resatr(1), resatr(2), resatr(3), resatr(4), resatr(5));
   allres = [allres ; resatr];
end

plot (recalls, allres(:,1),'g',recalls, allres(:,2),'m',recalls, allres(:,3), 'r', ...
    recalls, allres(:,4), 'b', recalls, allres(:,5), 'k');
legend ('LSH', 'LSH+frame', 'A-S binarized', 'A-S asymetric', 'A-S reconstruct');

